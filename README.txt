This library exposes the new Extractor2 API available in Confluence 5.2

Issues:
https://jira.atlassian.com/browse/CONF (component "Searching/Indexing")