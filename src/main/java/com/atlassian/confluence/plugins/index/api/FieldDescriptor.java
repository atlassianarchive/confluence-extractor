package com.atlassian.confluence.plugins.index.api;

/**
 * Represents an index field.
 */
public class FieldDescriptor
{
    private final String name;
    private final String value;
    private final Index index;
    private final Store store;

    public FieldDescriptor(String name, String value, Store store, Index index)
    {
        if (name == null || name.trim().isEmpty())
            throw new IllegalArgumentException("name is required.");
        if (value == null)
            throw new IllegalArgumentException("value is cannot be null.");
        if (store == null)
            throw new IllegalArgumentException("store is required.");
        if (index == null)
            throw new IllegalArgumentException("index is required.");

        this.name = name;
        this.value = value;
        this.store = store;
        this.index = index;
    }

    public String getName()
    {
        return name;
    }

    public String getValue()
    {
        return value;
    }

    public Index getIndex()
    {
        return index;
    }

    public Store getStore()
    {
        return store;
    }

    public static enum Index
    {
        NO,
        ANALYZED,
        NOT_ANALYZED
    }

    public static enum Store
    {
        NO,
        YES
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FieldDescriptor that = (FieldDescriptor) o;

        if (index != that.index) return false;
        if (!name.equals(that.name)) return false;
        if (store != that.store) return false;
        if (!value.equals(that.value)) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = name.hashCode();
        result = 31 * result + value.hashCode();
        result = 31 * result + index.hashCode();
        result = 31 * result + store.hashCode();
        return result;
    }
}
